from setuptools import setup

entry_points = '''
[zc.buildout]
zk = zc_zrs_rpm_recipes:ZKRecipe
'''

setup(
    name = "zc_zrs_rpm_recipes",
    package_dir = {'': 'src'},
    entry_points = entry_points,
    extras_require = dict(test=['manuel', 'zc.buildout', 'mock']),
    install_requires = [
        'ZConfig',
        'z3c.recipe.mkdir',
        'zc.buildout',
        'zc.metarecipe',
        'zc.monitor',
        'zc.recipe.deployment',
        'zc.recipe.rhrc',
        'zc.zdaemonrecipe',
        'zc.zodbrecipes',
        'zc.zk [static]'
        ]
    )
