
Remove zimagent monitoring configuration.

3.16.6 2015-03-07
=================

Included:

  nagios_performance = true

In CIMAA configs to get metrics.

(I screwed up the version number when I generated the rpm :( )

3.15.5 2015-02-18
=================

Fixed: zdaemon missconfiguration for blob server

3.15.4 2015-02-18
=================

- Use newer zc.zrs to get proper ordering of transaction-time queriesc

- Allow a larger heap size for blob servers.

- (Try to) filter out spurious output from GC script to avoid xrying
  wolf to sentry.

- Added CIMAA configurations for blob servers.

3.15.3 2015-02-16
=================

- Fixed: no zeopack script was installed

3.15.2 2015-02-07
=================

- Fixed, from new ZRS: zrs monitor didn't get primary address correctly.

- Fixed: zeo monitor was configured with the wrong path.

3.15.1 2015-02-03
=================

- Fixed: Monitor configurations were generated incorrectly.

- Fixed: Apparently, zim wants unique section names across files

3.15.0 2015-01-28
=================

- Use nagios monitors for ZIM and CIMAA.

- Made the default pack schedule weekly.

- Removed the old non-ZooKeeper-based meta recipe.

- Updated to zc.zrs-2.5.1 to avoid warning when loading tests.

3.14.1 2014-12-26
=================

Update zc.recipe.deployment to get correct pack script generation to
have correct permissions in cron.Xly.

3.14.0 2014-12-26
=================

Added support for packing using cron.weekly (or daily or monthly).

3.13.2 2014-08-28
=================

Update zc.zk (and kazoo) for less hangs. :/

3.13.1 2014-07-29
=================

Update kazoo for less chatter.

3.13.0 2014-07-28
=================

Don't use raven-cron.  We're now using it for all crons.

3.12.0 2014-06-10
=================

Added support for using multi-gc on single databases.

3.11.2 2014-02-21
=================

Get raven-cron from a separate RPM, since it needs Python 2.7.

3.11.1 2014-02-12
=================

Fixed: zc.zk was missing monitor.zcml.

3.11.0 2014-02-11
=================

- Use kazoo to talk to ZooKeeper.

3.10.1 2014-02-09
=================

- Fixed: Didn't build raven-cron correctly. :(

3.10.0 2014-02-06
=================

- Send pack errors to sentry.

- Fixed: The garbage collection script was missing some dependencies.

3.9.0 2013-12-27
================

- Separate option for enabling s3 blob storage on certain hosts
  without triggering s3 blob clients in applications.

3.8.0 2013-12-05
================

- option to enable s3 blob storage for certain hosts

3.7.0 2013-11-20
================

For zookeeper-based deployments:

- The ``perform-gc-if-secondary`` can now be a list of host names, in
  which case, we'll perform gc if the host is a secondary and it it's
  host name is in the list of secondary host names.

- Fixed: an incorrect packer was used, causing the usual slow
  garbage-collecting packer to be used.

3.6.1 2013-11-11
================

- Fixed: ZIM agent configs had incorrect socket-file paths.

3.6.0 2013-11-10
================

- Added a ``digest`` option to the zkdeployment-based meta recipe to
  avoid/control automatic server restarts.

3.5.1 2013-11-06
================

- When using zkdeployment, you can now specify:

  - a base directory (per database)

  - fixed ports

  - S3 blob storage

3.4.2 2013-11-04
================

- Fixed (via s3blobserver 0.0.5): The blob server generated lots of
  spurious log messages.

- Fixed (via zc.s3blobstorage 0.3.1): the s3blobserver monitor didn't
  output text on success.

- Fixed: The Java log-rotator used didn't clean up old files.

- Fixed: The zdaemon transcript log used the same log file as the java
  logger.  This caused colliding rotation.

- Fixed: The monitor configuration for the s3 blob server had the wrong
  path.


3.4.1 2013-10-30
================

Emergency release due to foolishly installing a centos 5 rpm on a
centos 4 box and neither yum or rpm allowing recovery without a new
release.

3.4.0 2013-10-30
================

- Added sbo configuration for s3 blob server monitor.

3.3.1 2013-10-29
================

- Fixed: ZRS ports were generated incorrectly when an s3 blob server
  was used.

3.3.0 2013-10-29
================

- Updated blob server to 0.0.3

- Support for aws credential configuration in ~zope/.aws.conf

- The --bs option now requires a size: BUCKET/PREFIX:PORT:SIZEMB

3.2.1 2013-10-25
================

- Updated blob server to 0.0.2

- Fixed: s3 blob server zookeeper path wasn't set correctly

3.2.0 2013-10-24
================

Added initial support for s3 blob storage.

To-do still:

- zkdeployment support

- better log handling

- monitoring


3.1.13 2013-10-05
=================

When deploying with zkdeployment, only include options actually read
in the rc script digest.

Fixed: database options weren't used in computing the run script digest,

3.1.12 2013-02-11
=================

Fixed: The path for zeopack in generated pack scripts were incorrect.

3.1.8 2012-11-29
================

Register with ZooKeeper by ID, rather than by host name.

3.1.7 2012-10-24
================

Fixed: Script paths in some generated configurations and scipts were
       wrong due to incorrect paths being hardcoded in the recipe (rather
       than using ${buildout:bin-directory}

3.1.6 2012-10-05
================

Updated zc.zk and zc-zookeeper-static

3.1.5 2012-10-04
================

Fixed: Non-ZooKeeper-based configuration of garbage collection didn't
       handle compression properly.

3.1.4 2012-10-03
================

- Added new monitor for use with zkdeployment.

3.0.0 2012-07-13
================

- Meta-recipe changed to use unix-domain sockets for the zc.monitor
  server. **IMPORTANT** don't install this on a Centos 4 box, as nc on
  Centos 4 doesn't support unix-domain sockets.

- zookeeper-deploy-compatible meta-recipe

2.9.10 2011-11-19
==================

Use ZODB 3.10.5, which has a conflict-resolution bug fix.

2.9.9 2011-11-17
==================

Use ZODB 3.10.4, which has a conflict-resolution bug fix.

2.9.6 2011-05-06
================

- Fixed a bug in the meta recipe that caused GC database config files
  to improperly handle compression.

- Updated to ZODB 3.9.3, which provides some minor server
  optimizations.
