import optparse
import os
import pwd
import re
import socket
import zc.metarecipe
import zc.zk
import zc_zrs_rpm_recipes
import zc_zrs_rpm_recipes.optionssignature

def get_storage_conf(type, options, base=None):
    result = ['  <%s>' % type]
    result.extend("    %s %s" % i for i in sorted(options.iteritems()) if i[1])
    if base is not None:
        result.append('  '+base.rstrip().replace('\n', '\n  '))
    return '\n'.join(result)+('\n  </%s>\n' % type)

zeo_conf = """
   <zeo>
      address :%(port)s
      invalidation-queue-size 10000
      invalidation-age 7200
      transaction-timeout 300
   </zeo>
   <zookeeper>
     connection zookeeper:2181
     path %(path)s/providers
     monitor-server ${deployment:run-directory}/%(sname)s-monitor.sock
   </zookeeper>

   %%import zc.zrs
   %%import zc.zlibstorage

%(storage_conf)s

   %%import zc.monitorlogstats
   <eventlog>
       level INFO
       <counter>
          format %%(name)s %%(message)s
       </counter>
       <logfile>
          path STDOUT
       </logfile>
   </eventlog>
"""
zdaemon_conf = """
   <runner>
     start-test-program ${buildout:bin-directory}/monitorcheck %s %s/providers
   </runner>
"""

cimaa_secondary_conf = """
[%(sname)s/secondary/zrs]
command = ${buildout:bin-directory}/zkzrs-nagios \\
    -M ${deployment:run-directory}/%(sname)s-monitor.sock \\
    -m zookeeper:2181 %(path)s
nagios_performance = true
[%(sname)s/secondary/zeo]
command = ${buildout:bin-directory}/zkzeo-nagios \\
    -M ${deployment:run-directory}/%(sname)s-monitor.sock \\
    -s ${deployment:run-directory}/%(sname)s.status \\
    -m zookeeper:2181 %(path)s/secondary/providers
nagios_performance = true
"""
cimaa_primary_conf = """
[%(sname)s/zeo]
command = ${buildout:bin-directory}/zkzeo-nagios \\
    -M ${deployment:run-directory}/%(sname)s-monitor.sock \\
    -s ${deployment:run-directory}/%(sname)s.status \\
    -m zookeeper:2181 %(path)s/providers
nagios_performance = true
"""

pack_sh = """
${buildout:bin-directory}/zeopack -d%s -t00 \\
  %s
"""

gc_conf_db = """
<zodb%(aname)s>
%(storage_conf)s</zodb>
"""

gc_command = """
${buildout:bin-directory}/multi-zodb-gc \\
    -d3 -lERROR %(extra)s \\
    %(files)s \\
    ${%(config)s:location} | grep -v '<zc.zodbdgc.Bad instance at'
"""

class ZKRecipe(zc.metarecipe.Recipe):

    def __init__(self, buildout, name, _):
        super(ZKRecipe, self).__init__(buildout, name, _)

        assert name.endswith('.0'), name # There can be only one.
        name = name[:-2]

        zk = self.zk = zc.zk.ZK('zookeeper:2181')

        path = '/' + name.replace(',', '/')
        options = zc_zrs_rpm_recipes.optionssignature.Wrapper(
            zk.properties(path))

        user = self.user = options.get('user', 'zope')

        self['deployment'] = dict(
            recipe = 'zc.recipe.deployment',
            name=name,
            user=user,
            )

        self.hostname = hostname = socket.getfqdn()
        self.is_primary = hostname == options['primary']

        self.pack_addrs = []
        snames = []
        self.digests = []
        self.cimaa_confs = []
        self.fspaths = {}
        pack = ''

        multi_gc = str(options.get('multi-gc', '')).strip()
        if multi_gc.startswith('='):
            path, sname = path.rsplit('/', 1)
            multi_gc = sname + multi_gc
        if multi_gc:
            multi_gc = multi_gc.split()
            for gcname in multi_gc:
                if gcname.startswith('='):
                    continue
                gcname = gcname.split('=')[0]
                self.database(path + '/' + gcname, snames, multi=True)

            perform_gc = options.get('perform-gc-if-secondary', False)
            if isinstance(perform_gc, (set, list, tuple)):
                perform_gc = hostname in perform_gc
            elif not isinstance(perform_gc, bool):
                raise TypeError(
                    'perform-gc-if-secondary must be a bool, list, set,'
                    ' or tuple', perform_gc)
            perform_gc = perform_gc and not self.is_primary

            if perform_gc:
                gc_conf_text = ''
                gc_command_extra = ''
                gc_command_files = []
                for sname in multi_gc:
                    if '=' in sname:
                        sname, aname = sname.split('=', 1)
                    else:
                        aname = sname
                    if sname:
                        storage_conf = get_storage_conf(
                            'zkzeoclient', dict(
                                zookeeper='zookeeper:2181',
                                server='%s/%s/providers' % (path, sname),
                                ))
                        storage_conf = get_storage_conf(
                            'zlibstorage', {}, storage_conf)

                        gc_conf_text += gc_conf_db % dict(
                            aname = aname and ' '+aname,
                            storage_conf = storage_conf,
                            )
                        gc_command_files.append(
                            '-f%s=%s' % (aname, self.fspaths[sname]))
                    else:
                        gc_command_extra += ' -i'+aname


                gc_conf_text = (
                    '%import zc.zlibstorage\n'
                    '%import zc.zkzeo\n'
                    + gc_conf_text)
                gc_command_extra += ' \\\n    -uzc.zlibstorage:decompress'

                self['gc.conf'] = dict(
                    recipe = 'zc.recipe.deployment:configuration',
                    deployment = 'deployment',
                    text = gc_conf_text,
                    )

                pack = gc_command % dict(
                    extra = gc_command_extra,
                    files = ' \\\n    '.join(gc_command_files),
                    config = 'gc.conf',
                    )

        else:
            self.database(path, snames)

        self['cimaa'] = dict(
            recipe = 'zc.recipe.deployment:configuration',
            name = '/etc/cimaa/monitors.d/%s.cfg' % name,
            text = ''.join(self.cimaa_confs),
            deployment = 'deployment',
            )

        pack = (
            pack_sh % (
                options.get('pack-days', default=3),
                ' \\\n  '.join(self.pack_addrs)
                )
            ) + pack

        self['pack.sh'] = dict(
            recipe = 'zc.recipe.deployment:configuration',
            deployment = 'deployment',
            text = pack,
            )
        pack_command = 'sh ${pack.sh:location}'
        pack_time = options.get('pack-time', 'weekly')
        if pack_time:
            if pack_time in ('daily', 'weekly', 'monthly'):
                run_path = '/etc/cron.%s/%s' % (
                    pack_time, re.sub('[^-_a-zA-Z0-9]','__', name))
                self['pack'] = dict(
                    recipe = 'zc.recipe.deployment:configuration',
                    deployment = 'deployment',
                    name = run_path,
                    text = '#!/bin/sh\n'+pack_command,
                    **{
                        'on-change': 'chmod +x '+run_path
                        })
            else:
                self['pack'] = dict(
                    recipe = 'zc.recipe.deployment:crontab',
                    deployment='deployment',
                    times = str(pack_time),
                    command = pack_command,
                    )

        self.digests.append(options.digest)
        self['rc'] = dict(
            recipe = 'zc.recipe.rhrc',
            deployment = 'deployment',
            parts = ' '.join(snames),
            chkconfig = '345 99 10',
            digest = str(options.get('digest', '')) or ''.join(self.digests),
            **{'process-management': 'true'}
            )

    def database(self, path, rcparts, multi=False):
        prefix, sname = path.rsplit('/', 1)
        rcparts.append(sname)
        prefix = prefix[1:]
        zeo_path = primary_path = path
        if not self.is_primary:
            zeo_path += '/secondary'

        options = zc_zrs_rpm_recipes.optionssignature.Wrapper(
            self.zk.properties(path))

        s3 = options.get("s3-trans")
        if s3:
            if options.get('s3'):
                raise ValueError("Don't use s3 and s3-trans together.")
            s3, s3re = s3.split()
            if not re.search(s3re, self.hostname):
                s3 = None
        else:
            s3 = options.get("s3")

        port = options.get("port", 0)
        base_dir = options.get("base-dir", "/home/databases")

        fspath = str(options.get(
            'path', os.path.join(base_dir, prefix, sname+'.fs')))
        self.fspaths[sname] = fspath
        self.mkdir(sname+'-database-dir', os.path.dirname(fspath))
        storage_conf = {
            'path': fspath,
            'pack-keep-old': 'false'
            }

        if multi:
            storage_conf['packer'] = (
                "zc.FileStorage:Packer(%s, transform='zc.zlibstorage:compress')"
                % ("2" if self.is_primary else "0")
                )

        filestorage_type = 'filestorage'
        if options.get('blobs', True):
            bpath = str(options.get(
                'blob-dir',
                os.path.join(base_dir, prefix, sname+'.blobs')))
            self.mkdir(sname+'-blob-dir', bpath)
            storage_conf['blob-dir'] = bpath


            ##################################################################
            # S3 Blob Server

            if s3:

                filestorage_type = "s3blobfilestorage"
                cache_path = os.path.join(base_dir, prefix, sname+'.blob-cache')
                self.mkdir(sname+'-blob-cache-dir', cache_path)
                s3_blobserver(
                    self,
                    s3,
                    deployment_name = prefix.replace('/', ','),
                    sname = sname,
                    blob_dir = bpath,
                    cache_directory = cache_path,
                    zookeeper = 'zookeeper:2181',
                    zkpath = primary_path + '/blobs/providers',
                    )
                rcparts.append(sname+'-s3blobserver')

            #
            ##################################################################

        storage_conf = get_storage_conf(
            'serverzlibstorage'
            if options.get('client-compression', True) else 'zlibstorage',
            {},
            get_storage_conf(
                'zrs', {
                    'zookeeper': 'zookeeper:2181',
                    'replicate-to':
                    zeo_path +
                    '/replication/providers',
                    },
                get_storage_conf(
                    filestorage_type,
                    storage_conf),
                )
            )

        if not self.is_primary:
            storage_conf = get_storage_conf('zrs', {
                'zookeeper': 'zookeeper:2181',
                'replicate-from': path + '/replication/providers',
                'keep-alive-delay': 60,
                }, storage_conf)

        if s3:
            storage_conf = "  %import zc.s3blobstorage\n\n" + storage_conf

        self[sname] = dict(
            recipe = 'zc.zodbrecipes:server',
            deployment = 'deployment',
            **{
                'zeo.conf': zeo_conf % dict(
                    sname = sname,
                    storage_conf = storage_conf,
                    path = zeo_path,
                    port = port,
                    ),
                'shell-script': 'true',
                'zdaemon.conf': zdaemon_conf % (
                    '${deployment:run-directory}/%s-monitor.sock' % sname,
                    zeo_path),
            })

        self.pack_addrs.append(
            '`${buildout:bin-directory}/monitoraddr'
            ' ${deployment:run-directory}/%s-monitor.sock'
            ' %s/providers`'
            % (sname, zeo_path))

        self.cimaa_confs.append(
            (cimaa_primary_conf if self.is_primary else cimaa_secondary_conf) %
            dict(sname = sname, path = path))

        self.digests.append(options.digest)

    def mkdir(self, name, path):
        self[name] = dict(
            recipe = 'z3c.recipe.mkdir',
            paths = path,
            user = self.user,
            group = self.user,
            )


s3_blobserver_conf_template = '''
akka {
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  loglevel = INFO
}

# Note to get debugging info, change s/INFO/DEBUG above and below.

s3blobserver {
  log4j = """
  log4j.rootLogger=INFO, LOGFILE
  log4j.appender.LOGFILE=org.apache.log4j.RollingFileAppender
  log4j.appender.LOGFILE.File= ${deployment:log-directory}/%(log)s
  log4j.appender.LOGFILE.MaxFileSize=10MB
  log4j.appender.LOGFILE.MaxBackupIndex=5
  log4j.appender.LOGFILE.layout=org.apache.log4j.PatternLayout
  log4j.appender.LOGFILE.layout.ConversionPattern=%%d{ISO8601} %%-5p %%c %%m%%n
  log4j.logger.org.apache.zookeeper=WARN
  """

  cache {
    same-file-system = true
    directory = %(cache_directory)s
    size = %(size)s
  }
  s3 {
    bucket = %(bucket)s
    prefix = %(prefix)s
  }
  committed {
    directory = %(blob_directory)s
    age = 1h
    poll-interval = 1h
  }
  server {
    port = %(port)s
    host = %(host)s
    path = %(path)s
    zookeeper = "%(zookeeper)s"
    zookeeper-data = """{"deployment": "%(id)s"}"""
  }
}
include file("/home/zope/.aws.conf")
'''

s3_blobserver_cimaa_template = """\
[%(sname)s-blobserver]
command =
  /opt/zrs/bin/s3-blob-server-monitor zookeeper:2181 \\
    %(path)s %(host)s
"""

def s3_blobserver(self,
                  s3,
                  deployment_name,
                  sname,
                  blob_dir,
                  cache_directory,
                  zookeeper,
                  zkpath,
                  ):
    host = socket.getfqdn()
    bucket, port, size = s3.split(':')
    bucket, prefix = bucket.split('/', 1)
    if not prefix.endswith('/'):
        prefix += '/'

    self[sname+'-s3blobserver.conf'] = dict(
        recipe = 'zc.recipe.deployment:configuration',
        deployment = "deployment",
        text = s3_blobserver_conf_template % dict(
            log=sname+'-s3blobserver.log',
            cache_directory = cache_directory,
            bucket = bucket,
            prefix = prefix,
            blob_directory = blob_dir,
            port = port,
            size = size,
            host = host,
            zookeeper = zookeeper,
            path = zkpath,
            id = deployment_name,
            ),
        )
    self[sname+'-s3blobserver'] = dict(
        recipe = 'zc.zdaemonrecipe',
        deployment = 'deployment',
        program = (
            '${buildout:parts-directory}/s3blobserver/bin/server'
            ' ${%s-s3blobserver.conf:location}' % sname),
        **{
            'shell-script': 'true',
            'zdaemon.conf': """
            <runner>
            transcript ${deployment:log-directory}/%s
            </runner>
            <environment>
            JVM_OPT -Xmx512m
            </environment>
            """ % (sname+'-s3blobserver.transcript'),
            }
        )

    self.cimaa_confs.append(s3_blobserver_cimaa_template % dict(
        sname = sname,
        path = zkpath,
        host = host,
        ))
