Configure a single server from data in a ZooKeeper tree.
========================================================

This doctests a recipe for configuring a single ZRS server that uses
ephemeral ports for all of it's TCP services and that get's it's
configuration from ZooKeeper.

We'll look at a series of examples, starting with the simplest.  We
have a tree that has a database node at /acme/main, where the database
acts as a primary::

  /example
    /app
      /main
        primary = 'storage1.example.com'
        blobs = False
        pack-time = '1 2 * * 6'

.. -> tree

    >>> import zc.zk
    >>> zk = zc.zk.ZK('zookeeper:2181')
    >>> zk.import_tree(tree)

We invoke the meta recipe with empty options, but with a name based on
a tree path and a deployment number:

    >>> import pkg_resources
    >>> ZKRecipe = pkg_resources.load_entry_point(
    ...     'zc_zrs_rpm_recipes', 'zc.buildout', 'zk')

    >>> from zc.metarecipe.testing import Buildout
    >>> import mock
    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,main.0', {})
    ... # doctests: +NORMALIZE_WHITESPACE
    [deployment]
    name = example,app,main
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /home/databases/example/app
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/main/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/main/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/main/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              pack-keep-old false
              path /home/databases/example/app/main.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,main.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/main/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/main/providers`
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 1 2 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 738fa9aa202b14d9e685b63967225d420bd28c81dee3ce8dedb7db176cb3386a
    parts = main
    process-management = true
    recipe = zc.recipe.rhrc


A number of options can be provided:

base-dir
   Base database directory.  This defaults to /home/databases

blobs
   Boolean indicating whether blobs are used. (Default: True)

blob-dir
   The path to the blob directory.

   Defaults to BASEDIR/PREFIX/NAME.blobs, where PREFIX is the
   part of the deployment name before the last comma and NAME is the
   part of the deployment name after the last comma.

   Note
     We haven't used this and probably won't. :/

s3
   Save blobs to s3.  The value is of the form: BUCKET/PREFIX:PORT:SIZE

path
   A file-storage path.

   Defaults to BASEDIR/PREFIX/NAME.fs, where PREFIX is the
   part of the deployment name before the last comma and NAME is the
   part of the deployment name after the last comma.

pack-days
   Number of days in the past to pack to. (Default: 3)

pack-time
   crontab time specification for when to pack. (Default: 1 2 * * 6)

client-compression
   Boolean indicating whether clients compress database
   records. (Default: true).


Now, we'll look at an example that exercises some of these::

  /example
    /app
      /main
        primary = 'storage1.example.com'
        blobs = True
        pack-days = 0
        pack-time = '2 3 * * 6'
        base-dir = '/mnt/ephemeral0/databases'
        port = 4242


.. -> tree

    >>> import zc.zk
    >>> zk = zc.zk.ZK('zookeeper:2181')
    >>> zk.import_tree(tree)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,main.0', {})
    [deployment]
    name = example,app,main
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /mnt/ephemeral0/databases/example/app
    recipe = z3c.recipe.mkdir
    user = zope
    [main-blob-dir]
    group = zope
    paths = /mnt/ephemeral0/databases/example/app/main.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/main/providers
         </runner>
    zeo.conf = <zeo>
            address :4242
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/main/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/main/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              blob-dir /mnt/ephemeral0/databases/example/app/main.blobs
              pack-keep-old false
              path /mnt/ephemeral0/databases/example/app/main.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,main.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/main/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d0 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/main/providers`
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 2 3 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 97a3e6478ba420d63fc5f43f4395ba6742a84befb79026412ce2dc09bdfaa6cb
    parts = main
    process-management = true
    recipe = zc.recipe.rhrc

If we are running on a secondary, we will get a slightly different
configuration.  Let's also get explicit with paths:

  /example
    /app
      /main
        primary = 'storage1.example.com'
        blob-dir = '/var/data/foo.blobs'
        path = '/mnt/ssd/foo.fs'
        pack-time = '1 2 * * 6'


.. -> tree

    >>> import zc.zk
    >>> zk = zc.zk.ZK('zookeeper:2181')
    >>> zk.import_tree(tree)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'secondary1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,main.0', {})
    [deployment]
    name = example,app,main
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /mnt/ssd
    recipe = z3c.recipe.mkdir
    user = zope
    [main-blob-dir]
    group = zope
    paths = /var/data/foo.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/main/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/main/secondary/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/main/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/main/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /var/data/foo.blobs
                pack-keep-old false
                path /mnt/ssd/foo.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,main.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -m zookeeper:2181 /example/app/main
      nagios_performance = true
      [main/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/main/secondary/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/main/secondary/providers`
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 1 2 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 167378355c48b651563995d6e62c37ac0bd28c81dee3ce8dedb7db176cb3386a
    parts = main
    process-management = true
    recipe = zc.recipe.rhrc

Multi-databases
===============

For multi-database configurations, a node is created with database subnodes::

  /example
     /app
       /databases : zrs
          primary = 'storage1.example.com'
          multi-gc = 'main= sessions ugc =zaam =transcoder'
          perform-gc-if-secondary = True
          pack-time = '1 2 * * 6'
          /main
            blob-dir = '/var/data/foo.blobs'
            path = '/mnt/ssd/foo.fs'
          /ugc
          /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

The node must have primary and multi-gc options.  The multi-gc option
consists of a set of white-space-separated tokens of one of 3 forms:

NAME
    The name of a database to be included in the multi-database.
    It's in-application name is the same as it's name.

NAME=APPNAME
    NAME is the name of a database to be included in the multi-database.
    APPNAME is it's in-application name and may be empty.  (It's
    common for an applications main database to be unnamed.)

=APPNAME
    The application name of a database outside the multi-database (for
    purposes of garbage collection) but that may be referenced from
    records of the databases being garbage collected.

The node may also have a perform-gc-if-secondary option, which
indicates whether a server should perform garbage collection if it's a
secondary.  It defaults to false.  Database specific options are
placed in the database nodes.

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctests: +NORMALIZE_WHITESPACE
    [deployment]
    name = example,app,databases
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /mnt/ssd
    recipe = z3c.recipe.mkdir
    user = zope
    [main-blob-dir]
    group = zope
    paths = /var/data/foo.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/main/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/main/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              blob-dir /var/data/foo.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /mnt/ssd/foo.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [sessions-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/sessions.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/sessions/providers
           monitor-server ${deployment:run-directory}/sessions-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/sessions/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              blob-dir /home/databases/example/app/databases/sessions.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /home/databases/example/app/databases/sessions.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [ugc-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [ugc-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/ugc.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [ugc]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/ugc/providers
           monitor-server ${deployment:run-directory}/ugc-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/ugc/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              blob-dir /home/databases/example/app/databases/ugc.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /home/databases/example/app/databases/ugc.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,databases.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/databases/main/providers
      nagios_performance = true
    <BLANKLINE>
      [sessions/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/sessions-monitor.sock \
          -s ${deployment:run-directory}/sessions.status \
          -m zookeeper:2181 /example/app/databases/sessions/providers
      nagios_performance = true
    <BLANKLINE>
      [ugc/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/ugc-monitor.sock \
          -s ${deployment:run-directory}/ugc.status \
          -m zookeeper:2181 /example/app/databases/ugc/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/providers`
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 1 2 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 167378355c48b651563995d6e62c37acd751713988987e9331980363e24189ced751713988987e9331980363e24189ce56d9d86d41be6994477340a9c1edd14a
    parts = main sessions ugc
    process-management = true
    recipe = zc.recipe.rhrc

Note that we didn't include any garbage-collection configuration,
because we're not a secondary.  Let's try that now:

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage2.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctests: +NORMALIZE_WHITESPACE
    [deployment]
    name = example,app,databases
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /mnt/ssd
    recipe = z3c.recipe.mkdir
    user = zope
    [main-blob-dir]
    group = zope
    paths = /var/data/foo.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/main/secondary/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/databases/main/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/databases/main/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /var/data/foo.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /mnt/ssd/foo.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [sessions-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/sessions.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/sessions/secondary/providers
           monitor-server ${deployment:run-directory}/sessions-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/databases/sessions/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/databases/sessions/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /home/databases/example/app/databases/sessions.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /home/databases/example/app/databases/sessions.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [ugc-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [ugc-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/ugc.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [ugc]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/ugc/secondary/providers
           monitor-server ${deployment:run-directory}/ugc-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/databases/ugc/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/databases/ugc/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /home/databases/example/app/databases/ugc.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /home/databases/example/app/databases/ugc.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [gc.conf]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = %import zc.zlibstorage
      %import zc.zkzeo
    <BLANKLINE>
      <zodb>
        <zlibstorage>
          <zkzeoclient>
            server /example/app/databases/main/providers
            zookeeper zookeeper:2181
          </zkzeoclient>
        </zlibstorage>
      </zodb>
    <BLANKLINE>
      <zodb sessions>
        <zlibstorage>
          <zkzeoclient>
            server /example/app/databases/sessions/providers
            zookeeper zookeeper:2181
          </zkzeoclient>
        </zlibstorage>
      </zodb>
    <BLANKLINE>
      <zodb ugc>
        <zlibstorage>
          <zkzeoclient>
            server /example/app/databases/ugc/providers
            zookeeper zookeeper:2181
          </zkzeoclient>
        </zlibstorage>
      </zodb>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,databases.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -m zookeeper:2181 /example/app/databases/main
      nagios_performance = true
      [main/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/databases/main/secondary/providers
      nagios_performance = true
    <BLANKLINE>
      [sessions/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/sessions-monitor.sock \
          -m zookeeper:2181 /example/app/databases/sessions
      nagios_performance = true
      [sessions/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/sessions-monitor.sock \
          -s ${deployment:run-directory}/sessions.status \
          -m zookeeper:2181 /example/app/databases/sessions/secondary/providers
      nagios_performance = true
    <BLANKLINE>
      [ugc/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/ugc-monitor.sock \
          -m zookeeper:2181 /example/app/databases/ugc
      nagios_performance = true
      [ugc/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/ugc-monitor.sock \
          -s ${deployment:run-directory}/ugc.status \
          -m zookeeper:2181 /example/app/databases/ugc/secondary/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/secondary/providers`
    <BLANKLINE>
      ${buildout:bin-directory}/multi-zodb-gc \
          -d3 -lERROR  -izaam -itranscoder \
          -uzc.zlibstorage:decompress \
          -f=/mnt/ssd/foo.fs \
          -fsessions=/home/databases/example/app/databases/sessions.fs \
          -fugc=/home/databases/example/app/databases/ugc.fs \
          ${gc.conf:location} | grep -v '<zc.zodbdgc.Bad instance at'
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 1 2 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 167378355c48b651563995d6e62c37acd751713988987e9331980363e24189ced751713988987e9331980363e24189ce56d9d86d41be6994477340a9c1edd14a
    parts = main sessions ugc
    process-management = true
    recipe = zc.recipe.rhrc

One more time on secondary without perform-gc-if-secondary::

  /example
     /app
       /databases : zrs
          primary = 'storage1.example.com'
          multi-gc = 'main= sessions ugc =zaam =transcoder'
          pack-time = '1 2 * * 6'
          /main
            blob-dir = '/var/data/foo.blobs'
            path = '/mnt/ssd/foo.fs'
          /ugc
          /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)
    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage2.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctests: +NORMALIZE_WHITESPACE
    [deployment]
    name = example,app,databases
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /mnt/ssd
    recipe = z3c.recipe.mkdir
    user = zope
    [main-blob-dir]
    group = zope
    paths = /var/data/foo.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/main/secondary/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/databases/main/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/databases/main/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /var/data/foo.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /mnt/ssd/foo.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [sessions-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/sessions.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/sessions/secondary/providers
           monitor-server ${deployment:run-directory}/sessions-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/databases/sessions/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/databases/sessions/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /home/databases/example/app/databases/sessions.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /home/databases/example/app/databases/sessions.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [ugc-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [ugc-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/ugc.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [ugc]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/ugc/secondary/providers
           monitor-server ${deployment:run-directory}/ugc-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/databases/ugc/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/databases/ugc/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /home/databases/example/app/databases/ugc.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /home/databases/example/app/databases/ugc.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,databases.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -m zookeeper:2181 /example/app/databases/main
      nagios_performance = true
      [main/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/databases/main/secondary/providers
      nagios_performance = true
    <BLANKLINE>
      [sessions/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/sessions-monitor.sock \
          -m zookeeper:2181 /example/app/databases/sessions
      nagios_performance = true
      [sessions/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/sessions-monitor.sock \
          -s ${deployment:run-directory}/sessions.status \
          -m zookeeper:2181 /example/app/databases/sessions/secondary/providers
      nagios_performance = true
    <BLANKLINE>
      [ugc/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/ugc-monitor.sock \
          -m zookeeper:2181 /example/app/databases/ugc
      nagios_performance = true
      [ugc/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/ugc-monitor.sock \
          -s ${deployment:run-directory}/ugc.status \
          -m zookeeper:2181 /example/app/databases/ugc/secondary/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/secondary/providers`
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 1 2 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 167378355c48b651563995d6e62c37acd751713988987e9331980363e24189ced751713988987e9331980363e24189ce2d11df89d1ef3a66bc6e8ff001f56a90
    parts = main sessions ugc
    process-management = true
    recipe = zc.recipe.rhrc

Rather than a boolean, perform-gc-if-secondary can be a list of
hostnames, in which case, gc will be performed if the machine is a
secondary and it's host name is one of the given host names::

  /example
     /app
       /databases : zrs
          primary = 'storage1.example.com'
          multi-gc = 'main= sessions ugc =zaam =transcoder'
          perform-gc-if-secondary = ['storage1.example.com', 'storage2.example.com']
          pack-time = '1 2 * * 6'
          /main
            blob-dir = '/var/data/foo.blobs'
            path = '/mnt/ssd/foo.fs'
          /ugc
          /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

This is to allow for teriary servers where we don't do gc.

We don't do gc on storage1 because it's primary:

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    [deployment]
    ...
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/providers`
    [pack]
    ...

We do gc on storage2 because it's secondary and it's in the list of
secondaries to do gc:

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage2.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    [deployment]
    ...
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/secondary/providers`
    <BLANKLINE>
      ${buildout:bin-directory}/multi-zodb-gc \
          -d3 -lERROR  -izaam -itranscoder \
          -uzc.zlibstorage:decompress \
          -f=/mnt/ssd/foo.fs \
          -fsessions=/home/databases/example/app/databases/sessions.fs \
          -fugc=/home/databases/example/app/databases/ugc.fs \
          ${gc.conf:location} | grep -v '<zc.zodbdgc.Bad instance at'
    [pack]
    ...

We don't do gc on storage3 even though it's secondary, because it's
not in the list of secondaries to do gc:

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage3.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    [deployment]
    ...
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/secondary/providers` \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/ugc-monitor.sock /example/app/databases/ugc/secondary/providers`
    [pack]
    ...

It's an error to give a string value (or any value type other than
bool, list, tuple, or set)::

  /example
     /app
       /databases : zrs
          primary = 'storage1.example.com'
          multi-gc = 'main= sessions ugc =zaam =transcoder'
          perform-gc-if-secondary = 'storage2.example.com'
          pack-time = '1 2 * * 6'
          /main
            blob-dir = '/var/data/foo.blobs'
            path = '/mnt/ssd/foo.fs'
          /ugc
          /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)
    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage3.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +ELLIPSIS +NORMALIZE_WHITESPACE
    Traceback (most recent call last):
    ...
    TypeError: ('perform-gc-if-secondary must be a bool, list, set, or tuple', 'storage2.example.com')


S3 Blob Storage
---------------

::

  /example
     /app
       /databases : zrs
          primary = 'storage1.example.com'
          multi-gc = 'main= sessions ugc =zaam =transcoder'
          perform-gc-if-secondary = True
          pack-time = '1 2 * * 6'
          /main
            s3 = 'blobs.example.com/example/app/databases:0:9999'
          /ugc
          /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    ...
    [main-blob-cache-dir]
    group = zope
    paths = /home/databases/example/app/databases/main.blob-cache
    recipe = z3c.recipe.mkdir
    user = zope
    [main-s3blobserver.conf]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = akka {
        loggers = ["akka.event.slf4j.Slf4jLogger"]
        loglevel = INFO
      }
    <BLANKLINE>
      # Note to get debugging info, change s/INFO/DEBUG above and below.
    <BLANKLINE>
      s3blobserver {
        log4j = """
        log4j.rootLogger=INFO, LOGFILE
        log4j.appender.LOGFILE=org.apache.log4j.RollingFileAppender
        log4j.appender.LOGFILE.File= ${deployment:log-directory}/main-s3blobserver.log
        log4j.appender.LOGFILE.MaxFileSize=10MB
        log4j.appender.LOGFILE.MaxBackupIndex=5
        log4j.appender.LOGFILE.layout=org.apache.log4j.PatternLayout
        log4j.appender.LOGFILE.layout.ConversionPattern=%d{ISO8601} %-5p %c %m%n
        log4j.logger.org.apache.zookeeper=WARN
        """
    <BLANKLINE>
        cache {
          same-file-system = true
          directory = /home/databases/example/app/databases/main.blob-cache
          size = 9999
        }
        s3 {
          bucket = blobs.example.com
          prefix = example/app/databases/
        }
        committed {
          directory = /home/databases/example/app/databases/main.blobs
          age = 1h
          poll-interval = 1h
        }
        server {
          port = 0
          host = storage1.example.com
          path = /example/app/databases/main/blobs/providers
          zookeeper = "zookeeper:2181"
          zookeeper-data = """{"deployment": "example,app,databases"}"""
        }
      }
      include file("/home/zope/.aws.conf")
    [main-s3blobserver]
    deployment = deployment
    program = ${buildout:parts-directory}/s3blobserver/bin/server ${main-s3blobserver.conf:location}
    recipe = zc.zdaemonrecipe
    shell-script = true
    zdaemon.conf = <runner>
                  transcript ${deployment:log-directory}/main-s3blobserver.transcript
                  </runner>
                  <environment>
                  JVM_OPT -Xmx512m
                  </environment>
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/main/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        %import zc.s3blobstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/main/replication/providers
            zookeeper zookeeper:2181
            <s3blobfilestorage>
              blob-dir /home/databases/example/app/databases/main.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /home/databases/example/app/databases/main.fs
            </s3blobfilestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [sessions-database-dir]
    group = zope
    paths = /home/databases/example/app/databases
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions-blob-dir]
    group = zope
    paths = /home/databases/example/app/databases/sessions.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [sessions]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/sessions-monitor.sock /example/app/databases/sessions/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/sessions/providers
           monitor-server ${deployment:run-directory}/sessions-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/sessions/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              blob-dir /home/databases/example/app/databases/sessions.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /home/databases/example/app/databases/sessions.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    ...
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,databases.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main-blobserver]
      command =
        /opt/zrs/bin/s3-blob-server-monitor zookeeper:2181 \
          /example/app/databases/main/blobs/providers storage1.example.com
    <BLANKLINE>
      [main/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/databases/main/providers
      nagios_performance = true
    <BLANKLINE>
      [sessions/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/sessions-monitor.sock \
          -s ${deployment:run-directory}/sessions.status \
          -m zookeeper:2181 /example/app/databases/sessions/providers
      nagios_performance = true
    <BLANKLINE>
      [ugc/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/ugc-monitor.sock \
          -s ${deployment:run-directory}/ugc.status \
          -m zookeeper:2181 /example/app/databases/ugc/providers
      nagios_performance = true
    ...


S3 Blob stoprage only for some hosts
------------------------------------

Transitioning to s3 blob storage is a little tricky, because the
storage server blob layout changes, The best approach is to ctreate a
new server with the new layout and the role swap to it. But we want a
single declarative configuration for an entire cluster.  We address
this by adding an s3-trans option that is like the s3 option
but:

- Has a regular expression for hosts to use s3 blob storage for.

- Doesn't trigger use of s3-based blob clients in applications.

::

 /example
   /app
     /databases : zrs
       primary = 'storage1.example.com'
       multi-gc = 'main= sessions ugc =zaam =transcoder'
       perform-gc-if-secondary = True
       pack-time = '1 2 * * 6'
       /main
         s3-trans = 'blobs.example.com/example/app/databases:0:9999 storage[12]'
       /ugc
       /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    ...
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/main/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        %import zc.s3blobstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/main/replication/providers
            zookeeper zookeeper:2181
            <s3blobfilestorage>
              blob-dir /home/databases/example/app/databases/main.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /home/databases/example/app/databases/main.fs
            </s3blobfilestorage>
          </zrs>
        </serverzlibstorage>
    ...


::

 /example
   /app
     /databases : zrs
       primary = 'storage1.example.com'
       multi-gc = 'main= sessions ugc =zaam =transcoder'
       perform-gc-if-secondary = True
       pack-time = '1 2 * * 6'
       /main
         s3-trans = 'blobs.example.com/example/app/databases:0:9999 storage[34]'
       /ugc
       /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    ...
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/databases/main/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/databases/main/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <serverzlibstorage>
          <zrs>
            replicate-to /example/app/databases/main/replication/providers
            zookeeper zookeeper:2181
            <filestorage>
              blob-dir /home/databases/example/app/databases/main.blobs
              pack-keep-old false
              packer zc.FileStorage:Packer(2, transform='zc.zlibstorage:compress')
              path /home/databases/example/app/databases/main.fs
            </filestorage>
          </zrs>
        </serverzlibstorage>
    ...

Don't use s3 and s3-trans together::

 /example
   /app
     /databases : zrs
       primary = 'storage1.example.com'
       multi-gc = 'main= sessions ugc =zaam =transcoder'
       perform-gc-if-secondary = True
       pack-time = '1 2 * * 6'
       /main
         s3 = 'blobs.example.com/example/app/databases:0:9999'
         s3-trans = 'blobs.example.com/example/app/databases:0:9999 storage[34]'
       /ugc
       /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    Traceback (most recent call last):
    ...
    ValueError: Don't use s3 and s3-trans together.


Controlling restarts by specifying a digest
-------------------------------------------

With storage servers, you might want tighter control over restarts.
Normally restarts (actually stops and starts) are triggered by changes
in the digest attribute of the rc part.  You can specify the digest,
so that the rc part isn't updated due to changes in other parts,
causing restarts.

::

  /example
     /app
       /databases : zrs
          primary = 'storage1.example.com'
          multi-gc = 'main= sessions ugc =zaam =transcoder'
          perform-gc-if-secondary = True
          digest = 42
          pack-time = '1 2 * * 6'
          /main
            s3 = 'blobs.example.com/example/app/databases:0:9999'
          /ugc
          /sessions

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,databases.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    ...
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = 42
    parts = main main-s3blobserver sessions ugc
    process-management = true
    recipe = zc.recipe.rhrc

multi-gc on single databases
============================

Multi-gc is much more efficient, so we'll often want to do it on
single databases. Maybe it should be the default. Anyway, foor now,
just add multi-gc = '='::


  /example
    /app
      /main
        primary = 'storage3.example.com'
        perform-gc-if-secondary = True
        multi-gc = '='
        pack-time = '1 2 * * 6'

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,main.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    name = example,app,main
    recipe = zc.recipe.deployment
    user = zope
    [main-database-dir]
    group = zope
    paths = /home/databases/example/app
    recipe = z3c.recipe.mkdir
    user = zope
    [main-blob-dir]
    group = zope
    paths = /home/databases/example/app/main.blobs
    recipe = z3c.recipe.mkdir
    user = zope
    [main]
    deployment = deployment
    recipe = zc.zodbrecipes:server
    shell-script = true
    zdaemon.conf = <runner>
           start-test-program ${buildout:bin-directory}/monitorcheck ${deployment:run-directory}/main-monitor.sock /example/app/main/secondary/providers
         </runner>
    zeo.conf = <zeo>
            address :0
            invalidation-queue-size 10000
            invalidation-age 7200
            transaction-timeout 300
         </zeo>
         <zookeeper>
           connection zookeeper:2181
           path /example/app/main/secondary/providers
           monitor-server ${deployment:run-directory}/main-monitor.sock
         </zookeeper>
    <BLANKLINE>
         %import zc.zrs
         %import zc.zlibstorage
    <BLANKLINE>
        <zrs>
          keep-alive-delay 60
          replicate-from /example/app/main/replication/providers
          zookeeper zookeeper:2181
          <serverzlibstorage>
            <zrs>
              replicate-to /example/app/main/secondary/replication/providers
              zookeeper zookeeper:2181
              <filestorage>
                blob-dir /home/databases/example/app/main.blobs
                pack-keep-old false
                packer zc.FileStorage:Packer(0, transform='zc.zlibstorage:compress')
                path /home/databases/example/app/main.fs
              </filestorage>
            </zrs>
          </serverzlibstorage>
        </zrs>
    <BLANKLINE>
    <BLANKLINE>
         %import zc.monitorlogstats
         <eventlog>
             level INFO
             <counter>
                format %(name)s %(message)s
             </counter>
             <logfile>
                path STDOUT
             </logfile>
         </eventlog>
    [gc.conf]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = %import zc.zlibstorage
      %import zc.zkzeo
    <BLANKLINE>
      <zodb>
        <zlibstorage>
          <zkzeoclient>
            server /example/app/main/providers
            zookeeper zookeeper:2181
          </zkzeoclient>
        </zlibstorage>
      </zodb>
    [cimaa]
    deployment = deployment
    name = /etc/cimaa/monitors.d/example,app,main.cfg
    recipe = zc.recipe.deployment:configuration
    text = [main/secondary/zrs]
      command = ${buildout:bin-directory}/zkzrs-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -m zookeeper:2181 /example/app/main
      nagios_performance = true
      [main/secondary/zeo]
      command = ${buildout:bin-directory}/zkzeo-nagios \
          -M ${deployment:run-directory}/main-monitor.sock \
          -s ${deployment:run-directory}/main.status \
          -m zookeeper:2181 /example/app/main/secondary/providers
      nagios_performance = true
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/main/secondary/providers`
    <BLANKLINE>
      ${buildout:bin-directory}/multi-zodb-gc \
          -d3 -lERROR  \
          -uzc.zlibstorage:decompress \
          -f=/home/databases/example/app/main.fs \
          ${gc.conf:location} | grep -v '<zc.zodbdgc.Bad instance at'
    [pack]
    command = sh ${pack.sh:location}
    deployment = deployment
    recipe = zc.recipe.deployment:crontab
    times = 1 2 * * 6
    [rc]
    chkconfig = 345 99 10
    deployment = deployment
    digest = d751713988987e9331980363e24189ce77dfab8b61c89f03601c38d298b21e14
    parts = main
    process-management = true
    recipe = zc.recipe.rhrc

Weekly (or daily or monthly) packs
==================================

Instead of a pack time, you can specify weekly (or daily or monthly).
In that case, packs will be done as part of the normal weekly (or
daily or monthly) processing, a major benefit being that multiple
packs on the same machine are done in sequence.

::

  /example
    /app
      /main
        primary = 'storage3.example.com'
        perform-gc-if-secondary = True
        multi-gc = '='
        pack-time = 'weekly'

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,main.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    ...
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/main/secondary/providers`
    <BLANKLINE>
      ${buildout:bin-directory}/multi-zodb-gc \
          -d3 -lERROR  \
          -uzc.zlibstorage:decompress \
          -f=/home/databases/example/app/main.fs \
          ${gc.conf:location} | grep -v '<zc.zodbdgc.Bad instance at'
    [pack]
    deployment = deployment
    name = /etc/cron.weekly/example__app__main
    on-change = chmod +x /etc/cron.weekly/example__app__main
    recipe = zc.recipe.deployment:configuration
    text = #!/bin/sh
      sh ${pack.sh:location}
    ...

The default is weekly::

  /example
    /app
      /main
        primary = 'storage3.example.com'
        perform-gc-if-secondary = True
        multi-gc = '='

.. -> tree

    >>> zk.import_tree(tree, trim=True)

    >>> with mock.patch('socket.getfqdn',
    ...         **{'return_value': 'storage1.example.com'}):
    ...     _ = ZKRecipe(Buildout(), 'example,app,main.0', {})
    ... # doctest: +NORMALIZE_WHITESPACE +ELLIPSIS
    [deployment]
    ...
    [pack.sh]
    deployment = deployment
    recipe = zc.recipe.deployment:configuration
    text = ${buildout:bin-directory}/zeopack -d3 -t00 \
        `${buildout:bin-directory}/monitoraddr ${deployment:run-directory}/main-monitor.sock /example/app/main/secondary/providers`
    <BLANKLINE>
      ${buildout:bin-directory}/multi-zodb-gc \
          -d3 -lERROR  \
          -uzc.zlibstorage:decompress \
          -f=/home/databases/example/app/main.fs \
          ${gc.conf:location} | grep -v '<zc.zodbdgc.Bad instance at'
    [pack]
    deployment = deployment
    name = /etc/cron.weekly/example__app__main
    on-change = chmod +x /etc/cron.weekly/example__app__main
    recipe = zc.recipe.deployment:configuration
    text = #!/bin/sh
      sh ${pack.sh:location}
    ...

.. cleanup

    >>> zk.close()
