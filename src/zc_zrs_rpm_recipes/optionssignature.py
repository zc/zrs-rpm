import hashlib

class Wrapper:

    def __init__(self, data):
        self.data = data
        self.accessed = set()

    def __getitem__(self, key):
        r = self.data[key]
        if isinstance(r, unicode):
            r = str(r)
        self.accessed.add(key)
        return r

    def get(self, key, default=None):
        try:
            return self[key]
        except KeyError:
            return default

    @property
    def digest(self):
        return hashlib.md5(repr(
            [(key, self.data[key]) for key in sorted(self.accessed)]
            )).hexdigest()
