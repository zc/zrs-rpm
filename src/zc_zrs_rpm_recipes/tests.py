
import unittest
import manuel.capture
import manuel.doctest
import manuel.testing
import re
import socket
import zc.zk.testing
import zope.testing.renormalizing
import zope.testing.setupstack

def setUp(test):
    zc.zk.testing.setUp(test, '', 'zookeeper:2181')

def test_suite():
    return unittest.TestSuite((
        manuel.testing.TestSuite(
            manuel.doctest.Manuel(
                checker = zope.testing.renormalizing.RENormalizing([
                    (re.compile(r'\S+/_TEST_/'), '/'),
                    (re.compile('FQDN'), socket.getfqdn()),
                    ]),
                ) + manuel.capture.Manuel(),
            'README.rst', 'optionssignature.txt',
            setUp=setUp,
            tearDown=zope.testing.setupstack.tearDown,
            ),
        ))
